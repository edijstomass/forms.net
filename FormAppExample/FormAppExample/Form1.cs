﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormAppExample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // This is called when button is clicked

        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello World!");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello from button 2!");
            button3.Visible = true;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello from button 3!");
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = DateTime.Now.ToString();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show(textBox1.Text);
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Button6_Click(object sender, EventArgs e)
        {
            DoChecked();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            DoChecked();
        }

      


        private void DoChecked()
        {
            button6.Enabled = checkBox1.Checked;
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            string sColor = "";

            if (radioButton1.Checked)
            {
                sColor = radioButton1.Text;
            }
            else if (radioButton2.Checked)
            {
                sColor = radioButton2.Text;
            }
            else if (radioButton3.Checked)
            {
                sColor = radioButton3.Text;
            }

            MessageBox.Show(sColor);
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            getColor(radioButton4);
            getColor(radioButton5);
            getColor(radioButton6);
            
        }


        private void getColor(RadioButton rdoButton)
        {
            if (rdoButton.Checked)
            {
                MessageBox.Show(rdoButton.Text);
            }
        }


       



    }
}
